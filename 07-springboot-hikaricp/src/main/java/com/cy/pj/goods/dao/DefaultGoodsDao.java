package com.cy.pj.goods.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Repository
public class DefaultGoodsDao implements GoodsDao{

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Map<String, Object>> findGoods(){
        Connection conn = null;
        Statement stat =null;
        ResultSet result =null;
        try {
            conn = dataSource.getConnection();
            String sql="select * from tb_goods";
            stat = conn.createStatement();
            result = stat.executeQuery(sql);
            List<Map<String,Object>> list=new ArrayList<>();

//            ResultSetMetaData metaData = result.getMetaData();
            while (result.next()){
//                Map<String, Object> map = new HashMap<>();
//                map.put("id", result.getInt(1));
//                map.put("name",result.getString(2));
//                map.put("remark",result.getString(3));
//                map.put("createdTime",result.getDate(4));
//                list.add(map);

//                int count = metaData.getColumnCount();
//                for (int i = 0; i < count; i++) {
//                   String columnName = metaData.getColumnLabel(i);
//                   map.put(columnName,result.getObject(columnName));
//                }

                list.add(rowMap(result));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }finally {
            close(result,stat,conn);
        }

    }

    private void close(ResultSet result, Statement stat, Connection conn) {
        try {
            if (result!=null){
                result.close();
                result=null;
            }
            if (result!=null){
                stat.close();
                stat=null;
            }
            if (result!=null){
                conn.close();
                conn=null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Object> rowMap(ResultSet result) throws SQLException {
        Map<String, Object> rowMap = new HashMap<>();
        ResultSetMetaData metaData = result.getMetaData();
        int count = metaData.getColumnCount();
        for (int i = 1; i < count; i++) {
            String columnName = metaData.getColumnLabel(i);
            rowMap.put(columnName,result.getObject(columnName));
        }

        return rowMap;
    }


}
