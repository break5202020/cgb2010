package com.cy.goods.dao;

import com.cy.pj.goods.dao.GoodsDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
public class DefaultGoodsDaoTests {
    @Autowired
    private GoodsDao goodsDao;

    @Test
    public void testFindGoods(){
        List<Map<String, Object>> list = goodsDao.findGoods();
        list.forEach(System.out::println);
    }
}
