package com.cy.pj.goods.service.impl;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class GoodsServiceImpl implements GoodsService {

    private static final Logger log=
            LoggerFactory.getLogger(GoodsServiceImpl.class);

    @Autowired
    private GoodsDao goodsDao;
    @Override
    public List<Goods> findGoods() {
        long t1=System.currentTimeMillis();
        log.info("开始时间:{}",t1);
        List<Goods> list = goodsDao.findGoods();
        long t2=System.currentTimeMillis();
//        System.out.println("时间："+t2);
        log.info("结束时间:{}",t2);
        return list;
    }
}
