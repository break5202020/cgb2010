package com.cy.pj.goods.dao.impl;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class GoodsDaoImpl implements GoodsDao {

    @Autowired
    private SqlSession sqlSession;

//    @Autowired
//    private GoodsDao goodsDao;

    @Override
    public List<Goods> findGoods() {
//        System.out.println("---------------------");
//        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
//        for (StackTraceElement s:stackTrace) {
//            System.out.println(s.getMethodName());
//        }



        //mybatis数据访问实现.
//        String namespace="com.cy.pj.goods.dao.GoodsDao";
        String namespace = this.getClass().getInterfaces()[0].getName();
        System.out.println("namespace="+namespace);

//        String methodName="findGoods";
        //获取当前线程的方法栈信息(每个线程都有一个私有的方法栈)
        //每个调用方法的相关信息会以StackTrace(栈帧：方法名，参数，返回值)
        String methodName=Thread.currentThread().getStackTrace()[1].getMethodName();
        System.out.println(methodName);

        String statement=namespace+"."+methodName;
        List<Goods> list = sqlSession.selectList(statement);
//        list.forEach(System.out::println);

//        List<Goods> list = goodsDao.findGoods();
        return list;
    }
}
