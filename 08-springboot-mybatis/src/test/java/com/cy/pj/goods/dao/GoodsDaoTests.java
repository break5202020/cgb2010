package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsDaoTests {

    @Autowired
    private GoodsDao goodsDao;

    @Test
    public void testFindGoods(){
        List<Goods> list = goodsDao.findGoods();
        list.forEach(System.out::println);
    }
}
