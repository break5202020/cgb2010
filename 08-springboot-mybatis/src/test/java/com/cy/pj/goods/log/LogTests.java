package com.cy.pj.goods.log;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogTests {
    private static final Logger log=
            LoggerFactory.getLogger(Logger.class);

    @Test
    public void testLog(){
        //日志级别：trace < debug < info < error
        log.info("--info--");
        log.debug("--debug--");
        log.trace("--trace--");
        log.error("--error--");
    }
}
