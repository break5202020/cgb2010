package com.cy.pj.goods.servcie;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsSerivceTests {

    @Autowired
    private GoodsService goodsService;

    @Test
    public void testFindGoods(){
        List<Goods> list = goodsService.findGoods();
        list.forEach(System.out::println);
    }
}
