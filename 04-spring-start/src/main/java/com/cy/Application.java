package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目在启动时要做哪些基础操作
 * 1.基于线程调用 I/O 从磁盘读取类，将其加载到内存，此时会基于类创建字节码对象(其类型为class类)
 * 2.基于class对象(字节码对象)读取类的配置信息(例如类的属性、方法、注解等)
 * 3.基于类的配置进行相应的配置存储(交给Spring管理的类的配置)
 * 4.基于类的配置借助BeanFactory创建类的实例(对象)，多个对象存储到Map<String,Object>
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
