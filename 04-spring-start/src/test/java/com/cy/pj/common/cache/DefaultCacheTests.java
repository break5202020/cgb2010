package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * SpringBoot工程中的单元测试类需要使用
 * @SpringBootTest注解进行描述
 */
@SpringBootTest
public class DefaultCacheTests {
    //spring中可以借助@Autowired注解描述属性,用于告诉spring这个属性的值由spring注入
    //has a (DefaultCacheTests 类中有一个DefaultCache类型的属性)
    @Autowired
    private DefaultCache defaultCache;
    //你还有其它方式可以获取defaultCache对象吗?
    @Autowired
    private BeanFactory beanFactory; //spring 会对其进行DI操作

    @Test
    void testDefaultCache(){
        System.out.println(defaultCache.toString());
    }

    @Test
    void testBeanFactory(){
        //基于工厂获取bean对象
        //这个bean对象在哪呢?(有可能在Bean池中-Map<String,Object>)
        //这个bean的名字一般默认为类名,首字母小写.
        System.out.println(beanFactory.getBean("defaultCache"));
    }
}
