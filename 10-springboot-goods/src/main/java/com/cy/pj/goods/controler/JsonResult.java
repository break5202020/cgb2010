package com.cy.pj.goods.controler;

import lombok.Data;

@Data
public class JsonResult {
    /**响应状态码
     * 1 表示ok,0表示error  */
    private Integer state=1;
    /**响应状态码对应的具体信息*/
    private String message="ok";
    /**响应数据(一般是查询时获取到的正确数据)*/
    private Object data;

    public JsonResult(){}
    public JsonResult(Throwable e){
        this.state=0;
        this.message=e.getMessage();
    }
    public JsonResult(Object data){
        this.data=data;
    }
    public JsonResult(String message){
        this.message=message;
    }
}
