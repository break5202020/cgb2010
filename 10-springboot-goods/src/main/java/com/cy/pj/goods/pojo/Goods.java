package com.cy.pj.goods.pojo;

import lombok.Data;

import java.sql.Date;
@Data
public class Goods {
    private Integer id;
    private String name;
    private String remark;
    private Date createdTime;

}
