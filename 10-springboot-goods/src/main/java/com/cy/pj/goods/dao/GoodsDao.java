package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface GoodsDao {
    /**商品查询：查询全部商品
     * @return List<Goods>     */
    List<Goods> findGoods();

    /**商品删除：根据id删除商品
     * @param id 根据id删除商品
     * @return int     */
    int deleteById(Integer id);

    List<Goods> findGoodsByName(String name);

    int insertObject(Goods goods);

    Goods findById(Integer id);

    int updateGoods(Goods goods);
}
