package com.cy.pj.goods.controler;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/goods/")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping("doFindGoods")
    public String doFindGoods(Model model){
        List<Goods> list = goodsService.findGoods();
        model.addAttribute("list",list);
        return "goods";
    }

    @RequestMapping("doDeleteById/{id}")
//    public String doDeleteById(@PathVariable Integer id){
//        goodsService.deleteById(id);
//        return "forward:/goods/doFindGoods";
    public String doDeleteById(@PathVariable Integer id,Model model){
        goodsService.deleteById(id);
        List<Goods> list = goodsService.findGoods();
        model.addAttribute("list",list);
        return "goods";
    }

    @RequestMapping("doFindByName")
    public String doFindByName( String name,Model model){
        List<Goods> list = goodsService.findGoodsByName(name);
        model.addAttribute("list",list);
        return "goods";
    }


    @RequestMapping("doLoadAddUI")
    public String doLoadAddUI(){
        return "goods-add";
    }

    @RequestMapping("doSaveGoods")
    public String doSaveGoods(Goods goods,Model model){
        goodsService.saveGoods(goods);
//        List<Goods> list = goodsService.findGoods();
//        model.addAttribute("list",list);
//        return "goods";

        //重定向
        return "redirect:/goods/doFindGoods";
    }

    @GetMapping("doFindById/{id}")
    public String doFindById(@PathVariable Integer id,Model model){
        Goods goods = goodsService.findById(id);
        model.addAttribute("goods",goods);
        return "goods-update";
    }

    @RequestMapping("doUpdateObject")
    public String doUpdateObject(Goods goods,Model model){
        goodsService.updateGoods(goods);
        List<Goods> list = goodsService.findGoods();
        model.addAttribute("list",list);
        return "goods";
    }


    @GetMapping("doFindJsonGoods")
    @ResponseBody
    public JsonResult doFindJsonGoods(){
        return new JsonResult(goodsService.findGoods());
    }

}
