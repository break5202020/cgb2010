package com.cy.pj.goods.controler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
//@ControllerAdvice
//@ResponseBody
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public JsonResult doHandlerException(RuntimeException e){
        log.error("RuntimeException.Exception{}",e.getMessage());
        return new JsonResult(e);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public JsonResult doHandlerException(IllegalArgumentException e){
        log.error("IllegalArgumentException.Exception{}",e.getMessage());
        return new JsonResult(e);
    }
}
