package com.cy.pj.goods.service;

import com.cy.pj.goods.pojo.Goods;

import java.util.List;

public interface GoodsService {
    /**商品查询：查询全部商品
     * @return List<Goods>     */
    List<Goods> findGoods();

    /**商品删除：根据id删除商品
     * @param id 根据id删除商品
     * @return int     */
    int deleteById(Integer id);

    List<Goods> findGoodsByName(String name);

    int saveGoods(Goods goods);

    Goods findById(Integer id);

    int updateGoods(Goods goods);
}
