package com.cy.pj.goods.service.impl;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<Goods> findGoods() {
        List<Goods> list = goodsDao.findGoods();
        return list;
    }

    @Override
    public int deleteById(Integer id) {
        int rows = goodsDao.deleteById(id);
        if (rows<1){
            throw new IllegalArgumentException("记录可能不存在");
        }
        return rows;
    }

    @Override
    public List<Goods> findGoodsByName(String name) {
        List<Goods> list = goodsDao.findGoodsByName(name);
        return list;
    }

    @Override
    public int saveGoods(Goods goods) {
        int rows = goodsDao.insertObject(goods);
        return rows;
    }

    @Override
    public Goods findById(Integer id) {
        if (id==null || id<1){
            throw new IllegalArgumentException("id错误");
        }
        Goods goods = goodsDao.findById(id);
        if (goods==null){
            throw new RuntimeException("记录可能不存在");
        }
        return goods;
    }

    @Override
    public int updateGoods(Goods goods) {
        int rows = goodsDao.updateGoods(goods);
        if (rows<1){
            throw new IllegalArgumentException("记录可能不存在");
        }
        return rows;
    }
}
