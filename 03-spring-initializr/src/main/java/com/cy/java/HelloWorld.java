package com.cy.java;

public class HelloWorld {
    /**
     * main方法是程序解释执行的入口方法
     * @param args main方法运行时，由jvm传入的运行数据
     *
     *  JVM参数
     *  -XX:+TraceClassLoading  打印类加载执行顺序
     */
    public static void main(String[] args) {
        System.out.println("HelloWorld");
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }
        System.out.println("你想做什么！");
    }
}
