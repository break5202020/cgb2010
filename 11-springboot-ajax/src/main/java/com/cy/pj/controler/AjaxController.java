package com.cy.pj.controler;

import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class AjaxController {

    private List<Map<String,String>> oList = new ArrayList<>();

    public AjaxController(){
        Map<String,String> map = new HashMap<>();
        map.put("id","1");
        map.put("name","李四");
        map.put("remark", "张三的老表");
        oList.add(map);
        map = new HashMap<>();
        map.put("id", "2");
        map.put("name","老张");
        map.put("remark", "张三，老张，五百年前是一家");
        oList.add(map);
    }

    //多个url映射为一个方法,有空值查询
    @GetMapping(path={"/doAjaxGet/{name}","/doAjaxGet"})
    public List<Map<String,String>> doAjaxGet(@PathVariable(required = false) String name){
        if (name==null || "".equals(name)) return oList;
        List<Map<String, String>> objectMap = new ArrayList<>();
        for (Map<String,String> map:oList){
            if (map.get("name").contains(name)){
                objectMap.add(map);
            }
        }
        return objectMap;
    }

    @PostMapping("/doAjaxPost")
    public String doAjaxPost(@RequestParam Map<String,String> map){
        oList.add(map);
        return "save ok";
    }

    //接收post 请求方式Json格式数据
    @PostMapping("/doAjaxPostJson")
    public String doAjaxPostJson(@RequestBody Map<String,String> map){
        oList.add(map);
        return "save ok";
    }

    @PutMapping("/doAjaxUpdate")
    public String doAjaxUpdate(@RequestParam Map<String,String> updateMap){
        Iterator it = oList.iterator();
        while (it.hasNext()){
            Map<String,String> map = (Map<String,String>) it.next();
            if (map.get("id").equals(updateMap.get("id"))){
                map.put("name", updateMap.get("name"));
                map.put("remark", updateMap.get("remark"));
            }
        }
        return "update ok";
    }

    @DeleteMapping("/doAjaxDelete")
    public String doAjaxDelete(String id){
        Iterator it = oList.iterator();
        while (it.hasNext()){
            Map<String,String> map = (Map<String,String>) it.next();
            if (map.get("id").equals(id)){
                it.remove();
            }
        }
        return "delete ok";
    }



}
