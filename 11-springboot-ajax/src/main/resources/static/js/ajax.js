function ajaxGet(url,params,callback){//封装ajax get 请求模板代码
    //1.create XHR object
    let xhr=new XMLHttpRequest();
    //2.set onreadystatechange
    xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
            if(xhr.status==200){
                callback(xhr.responseText);
            }
        }
    }
    //3.open connection
    xhr.open("GET",`${url}/${params}`,true);
    //4.send request
    xhr.send();
}

function ajaxPost(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //3.send request
    xhr.send(params);
}
//向服务端提交json格式字符串
function ajaxPostJSON(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Content-Type","application/json");
    //3.send request
    xhr.send(JSON.stringify(params));//将json对象转换为json格式字符串提交到服务端
}
function createXHR(callback){
    //1.create XHR object
    let xhr=new XMLHttpRequest();
    //2.set onreadystatechange
    xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
            if(xhr.status==200){
                callback(xhr.responseText);
            }
        }
    }
    return xhr;
}
function ajaxPut(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("put",url,true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //3.send request
    xhr.send(params);
}
function ajaxDelete(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=createXHR(callback);
    //2.open connection
    xhr.open("delete",url,true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    //3.send request
    xhr.send(params);
}