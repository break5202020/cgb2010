package com.cy.pj.health.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**此对象应用于处理DispathcherServlet(SpringMVC中的处理器)交给它的请求 */
@Controller //此注解描述的对象为Controller请求的处理器对象，我们通常称之为hander
public class TemplateController {
    /**
     * @RequestMapping 注解描述方法时用于定义请求url到handler中方法的映射
     * @return view 的名字
     * 请求地址：http://localhost/doTemplateUI
     * 请求的的简易处理过程
     * 1)基于域名获取ip地址(例如：127.0.0.1)
     * 2)基于ip地址找到网络中计算机(ip地址是网络中计算机的唯一标识)
     * 3)基于端口找到tomcat服务(port是计算机中应用的唯一标识)
     * 4)tomcat服务会基于thread并借助io读取网络http协议中请求数据
     * 5)tomcat内部会创建请求(request)和响应(response)对象，用于封装请求和响应信息
     * 6)tomcat会调用Filter对象对request数据进行过滤
     * 7)Filter对request过滤以后，会将请求交给DispatcherServlet进行处理？
     * 8)DispatcherServlet读取请求中的url，并基于url从requestMapping中找到对应的handler以及方法
     * 9)DispatcherServlet基于反射技术调用handler方法
     * 10)DispatcherServlet会将handler方法返回的view等相关信息进行封装，然后交给视图解析器进行解析
     * 11)ViewResolver视图解析器对为view name添加前缀，后缀，并将model中数据填充到view中
     * 12)DispatcherServlet 将视图解析器解析的结果封装到response对象，并将其响应到客户端。
     * 说明：
     * 1)此方法是DispatcherServlet基于反射技术进行调用？
     * 2)反射技术的最大优势？不能预知未来，但可以驾驭未来。
     */
    @RequestMapping("doTemplate")
    public String doTemplate(Model model){
        model.addAttribute("msg","Template page");
        return "default";
        //思考？
        //1)这里的返回值会交给谁？当然是方法调用者(DispatcherServlet)
        //2)dispacherServlet会将方法的返回值封装到ModelAndView对象中
        //3)将ModelAndView对象交给视图解析器(ViewResolver)进行解析
        //4)视图解析器对为view name添加前缀，后缀，并将model中数据填充到view中
    }

    @RequestMapping("doTemplate2")
    public ModelAndView doTemplate2(ModelAndView mv){
        mv.addObject("msg","Template page");
        mv.setViewName("default");
        return mv;
    }

}
