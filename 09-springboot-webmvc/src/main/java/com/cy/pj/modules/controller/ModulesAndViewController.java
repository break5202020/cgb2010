package com.cy.pj.modules.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**此对象应用于处理DispathcherServlet(SpringMVC中的处理器)交给它的请求 */
@Controller //此注解描述的对象为Controller请求的处理器对象，我们通常称之为hander
public class ModulesAndViewController {

    @RequestMapping("doTemplate3")
    public String doTemplate(Model model){  //model底层是一个map
        //model是一个view中要呈现的数据的一个对象
        model.addAttribute("msg","Template page");
        return "default";
    }

    @RequestMapping("doTemplate4")
    public ModelAndView doTemplate2(ModelAndView mv){
        mv.addObject("msg","Template page");
        mv.setViewName("default");
        return mv;
    }

}
