package com.cy.pj.modules.controller;

import com.cy.pj.modules.pojo.RequestParamter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 测试请求中的参数处理过程
 */
@RestController
@RequestMapping("/")
public class ParamObjectControler {
    // http://localhost/doParam01?id=1
    @RequestMapping("doParam01")
    public String doMethodParam(HttpServletRequest request){
        String idStr = request.getParameter("id");
        //假如我们后面业务需要的是一个整数,此时还需要进行类型转换
        Integer id=Integer.parseInt(idStr);
        //后续可以基于整数id值执行后面的业务了,例如删除,查找...
        return "request params id: "+id;
    }

    // http://localhost/doParam02?id=2
    //当使用方法参数直接获取请求数据时,方法参数名要与请求参数名相同
    @RequestMapping("doParam02")
    public String doMethodParam(Integer id){ //框架的力量
        return "request params id: "+ id;
    }

    // http://localhost/doParam03?ids=1,2,3,4
    @RequestMapping("doParam03")
    public String doMethodParam(Integer[] ids){
        return "request params id: "+ Arrays.toString(ids);
    }

    // http://localhost/doParam04?time=2020/12/31
    //假如方法要接收日期格式对象可以通过@DateTimeFormat注解指定可接收的日期格式
    //默认格式为yyyy/MM/dd
    @RequestMapping("doParam04")
    public String doMethodParam(Date time){
        return "request params id: "+ time;
    }

    // http://localhost/doParam05?time=2020-12-31
    @RequestMapping("doParam05")
    public String doMethodParam5(
            @DateTimeFormat(pattern = "yyyy-MM-dd") Date time){
        return "request params id: "+ time;
    }

    // http://localhost/doParam06?name=tony&page=2
    // http://localhost/doParam06?username=tony&page=2
    //假如在方法参数中需要指定某个参数的值必须在请求参数中有传递
    //@RequestParam注解用于描述方法参数,用于定义参数规则
    //1)方法参数变量的值,来自哪个请求参数,例如@RequestParam("username")
    //2)方法参数变量是否要必须传值 例如@RequestParam(required = true)
    //@RequestMapping(value="/doParam06",method = RequestMethod.GET)
    @GetMapping("doParam06")
    public String doMethodParam(
//            String name,
//            @RequestParam("username") String name,
            @RequestParam(value="name",required = false) String name,
            @RequestParam(required = true) Integer page){
        return "request params id: "+ name+":"+page;
    }

    // http://localhost/doParam07?name=tony
    @GetMapping("doParam07")
    public String doMethodParam(RequestParamter param){
        return "request params id: "+ param.toString();
    }

    // http://localhost/doParam08?name=tony&ids=1,2,3,4
    @GetMapping("doParam08")
    public String doMethodParam(RequestParamter param,Integer[] ids){
        return "request params id: "+ param.toString()+" ids="+Arrays.toString(ids);
    }

    //使用Postman工具

    // http://localhost/doParam09
    @PostMapping("doParam09")
    public String doMethodParam9(RequestParamter param, Integer[] ids){
        return "request params id: "+ param.toString()+" ids="+Arrays.toString(ids);
    }

    // http://localhost/doParam10
    @PostMapping("doParam10")
    public String doMethodParam(@RequestParam Map<String,Object> map){
        return "request params id: "+ map.toString();
    }

    // http://localhost/doParam11
    @PostMapping("doParam11")
    public String doMethodParam11(@RequestBody Map<String,Object> map){
        return "request params id: "+ map.toString();
    }

    // http://localhost/doParam12
    @PostMapping("doParam12")
    public String doMethodParam12(@RequestBody RequestParamter param){
        return "request params id: "+ param.toString();
    }

    // http://localhost/doParam13
    @GetMapping("doParam13/{id}/{name}")
    public String doMethodRestUrlParam(
            @PathVariable Integer id,String name){
        return "request params id: "+ id+","+name;
    }

}
