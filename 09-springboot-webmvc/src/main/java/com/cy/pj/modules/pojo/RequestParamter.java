package com.cy.pj.modules.pojo;

public class RequestParamter {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RequestParamter{" +
                "name='" + name + '\'' +
                '}';
    }
}
