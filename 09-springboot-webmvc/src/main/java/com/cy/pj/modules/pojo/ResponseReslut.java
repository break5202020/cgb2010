package com.cy.pj.modules.pojo;
/**基于此对象封装响应结果*/
public class ResponseReslut {
    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseReslut{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
