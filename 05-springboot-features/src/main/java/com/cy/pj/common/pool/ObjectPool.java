package com.cy.pj.common.pool;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 假设这是一个对象池
 * FAQ?
 * 1)当项目的启动类在启动时会将此类加载到内存吗?
 * 会,但是有条件,包结构必须正确.可以通过JVM参数检测类是否被加载了: -XX:+TraceClassLoading
 * 2)项目的启动类在启动会默认创建ObjectPool类型的实例吗?会的,可以通过构造方法进行校验.
 * 3)思考,对于一个池对象而言,相对与普通对象占用的资源是多还是少?多,但这个对象暂时不使用,
 * 又占用很多的资源是否会影响性能呢?
 */
//@Scope("prototype")
@Lazy //延迟对象的实例的创建,用的时候再去创建.
@Component
public class ObjectPool {
    public ObjectPool(){
        System.out.println("ObjectPool()");
    }
    @PostConstruct
    public void init(){
        System.out.println("init()");
    }

    @PreDestroy
    public void close(){
        System.out.println("destroy()");
    }
}
